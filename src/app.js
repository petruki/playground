const express = require('express');
const expressip = require('express-ip');
const path = require('path');
const locationRouter = require('./routers/location');

const app = express();

app.use(expressip().getIpInfoMiddleware);
app.use(express.json())
app.use(locationRouter)

module.exports = app