const express = require('express')
const router = new express.Router()

router.get('/location/whereami', (req, res) => {
    const ipInfo = req.ipInfo;
    
    var ip = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        (req.connection.socket ? req.connection.socket.remoteAddress : null)

    var message = `Hey, you are browsing from ${ipInfo.city} - ${ipInfo.country} and your IP address is: ${ip}`
    res.send(message)
})

router.get('/location/ip', (req, res) => {
    const ipInfo = req.ipInfo;
    
    var ip = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        (req.connection.socket ? req.connection.socket.remoteAddress : null)

    res.send(ip)
})

module.exports = router