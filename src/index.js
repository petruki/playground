const app = require('./app')
const port = process.env.PORT

app.listen(port, function () {
    console.log(`Express started on http://localhost:${port}; press Ctrl+C to terminate!!!`);
});